options(bitmapType='cairo')
options(device='cairo')

#voor PCF
# setwd('c:/Jibes/RWS-cta/')
port <- Sys.getenv('PORT')
if (nchar(port)==0){
  port = "8090"
}
print(port)

shiny::runApp('./app', host = '0.0.0.0', port = as.numeric(port))
